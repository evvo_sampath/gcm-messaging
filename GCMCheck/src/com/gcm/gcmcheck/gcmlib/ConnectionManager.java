package com.gcm.gcmcheck.gcmlib;

import java.util.HashMap;
import org.apache.http.HttpEntity;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import android.content.Context;
 
public class ConnectionManager {
	public enum ConnectionType {ConnectionTypePost,ConnectionTypeSoap,ConnectionTypeGet};
	private ConnectionType connectionType;
	private RequestParams connectionVariables;
	private AsyncHttpResponseHandler asyncHttpResponseHandler;
	private AsyncHttpClient client = new AsyncHttpClient();	
	
	public void connectionHandler(Context context,HashMap<String,String> connectionVariables,String serverURL,ConnectionType connectionType,HttpEntity entity,AsyncHttpResponseHandler asyncHttpResponseHandler) throws Exception{					
		if(connectionVariables!=null)
		this.connectionVariables = new RequestParams(connectionVariables);
		this.connectionType = connectionType;
		this.asyncHttpResponseHandler = asyncHttpResponseHandler;
		if(this.connectionType == ConnectionType.ConnectionTypePost&&entity==null){
			post(serverURL,this.connectionVariables,this.asyncHttpResponseHandler);
		}else if(this.connectionType == ConnectionType.ConnectionTypePost){
			post(context,serverURL,entity,"text/xml",this.asyncHttpResponseHandler);
		}
		else if(this.connectionType == ConnectionType.ConnectionTypeGet){
			get(serverURL,this.connectionVariables,this.asyncHttpResponseHandler);
		}
	}
	private void get(String url, RequestParams params,AsyncHttpResponseHandler responseHandler) {
		client.get(getAbsoluteUrl(url), params, responseHandler);
	}
	private void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
		client.post(getAbsoluteUrl(url), params, responseHandler);
	}
	private  void post(Context context,String url,HttpEntity entity,String contentType,AsyncHttpResponseHandler responseHandler){
		client.post(context, url, entity, contentType, responseHandler);
	}
	private String getAbsoluteUrl(String relativeUrl) {	
		return relativeUrl;		
	}
}