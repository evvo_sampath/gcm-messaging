package com.gcm.gcmcheck.gcmlib;

import java.util.HashMap;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.widget.Toast;

import com.gcm.gcmcheck.gcmlib.ConnectionManager.ConnectionType;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class SendGCMMessage extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		ProgressDialog processdialog = ProgressDialog.show(SendGCMMessage.this, "", "Sending GCM message...", true);
		processdialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);	  		
		processdialog.getWindow().setGravity(Gravity.CENTER);
		if(!getIntent().getStringExtra("register_id").equals("")){
			sendPushNotification();
		}else{
			Toast.makeText(getApplicationContext(), "GCM Registration id is empty", Toast.LENGTH_LONG).show();
			setResult(RESULT_CANCELED);
		}
	}
	private void sendPushNotification(){
		if(ConnectionCheck.checkOnline(getApplicationContext())){
			try {
				HashMap<String, String> postValues = new HashMap<String, String>();
				postValues.put(GcmConstants.GCM_KEY, getIntent().getStringExtra("register_id"));
				postValues.put(GcmConstants.GCM_MESSAGE, getIntent().getStringExtra("gcm_message"));
				postValues.put(GcmConstants.GCM_API, GcmConstants.GCM_API_KEY);
				
				Log.d("postValues", postValues.toString());
				ConnectionManager test = new ConnectionManager();
				
				test.connectionHandler(SendGCMMessage.this, postValues, GcmConstants.NOTIFICATION, ConnectionType.ConnectionTypePost, null, 
					  new AsyncHttpResponseHandler(){
				  	@Override
					public void onSuccess(String response) {
						// success functionality
				  		setResult(RESULT_OK);
				  		finish();
					}	
				  	@Override
				  	public void onFailure(Throwable error) {
						setResult(RESULT_CANCELED);
						finish();
					}
				});
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			// no Internet connection
			Log.d("internect check", "no internet connection, handle this with custom dialog for settings");
		}
	}
}
