package com.gcm.gcmcheck.gcmlib;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionCheck {

	public static boolean checkOnline(Context ctx){
		try{
		    ConnectivityManager cm =
		        (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
		    NetworkInfo netInfo = cm.getActiveNetworkInfo();
		    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
		        return true;
		    }
		    return false;
		}catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
