package com.gcm.gcmcheck.gcmlib;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import com.gcm.gcmcheck.R;

public class GCMMessageView extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.messageview);

		// Retrive the data from GCMIntentService.java
		Intent i = getIntent();
		String message = i.getStringExtra("message");
		Log.d("message", message);
		// Set the data into TextView
		((TextView) findViewById(R.id.message)).setText(message);
	}
}