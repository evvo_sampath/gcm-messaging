package com.gcm.gcmcheck.gcmlib;

public class GcmConstants {

	public static final String PREFERENCE_NAME = "EVVOGCM";
	public static final String GCM_REG_ID = "gcm_device_id";
	public static final String GCM_DEVICE_REGISTERED = "Registered_without_GCM_ID";
	
	public static final String GCM_KEY = "register_id";
	public static final String GCM_MESSAGE = "message";
	public static final String GCM_API = "api_key";
	
	// replace your PROJECT ID from Google API into SENDER_ID
	public static final String SENDER_ID = "1042489781848";
	
	// replace Server address for sending gcm messages - php script
	public static final String NOTIFICATION = "http://dev.evvolutions.com/gcm/gcmcheck-message.php";
	
	// replace google api key here (key reference to any)
	public static final String GCM_API_KEY = "AIzaSyDuLP7YUOp38dOKupdtK4oHaD3LseABe7o";
	
	// GCM register ID update to server for feature purpose
	public static final String GCM_UPDATE = "server api to update gcm registration details";
	
	public static final int REQUEST_SEND_GCM = 1;
}
