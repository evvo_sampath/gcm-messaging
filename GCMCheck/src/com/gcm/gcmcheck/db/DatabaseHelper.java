package com.gcm.gcmcheck.db;

/*********************************************************************************************************
 * Class	: DatabaseHelper
 * Type		: SQLiteOpenHelper
 * Date		: 19 Set 2013
 * 
 * Description:
 * 
 * SQLITE database
 * 
 * Values are stored in db and got back with sql queries
 * 
 * We are storing an values in local database to show the datas
 * when the user request to see his activities
 *  
 *  This is done with device specific not with user specific
 * 
 * ********************************************************************************************************/


import java.util.Vector;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DatabaseHelper extends SQLiteOpenHelper {
	private static String dbName = "GCMService";
	private SQLiteDatabase database;
    private static int dbVersion = 1;
    
    private String tblNotification = "notification", message = "message", msgDate="msg_date";
    
    public DatabaseHelper(Context context) {
        super(context, dbName, null, dbVersion);
    }
/**
 * Will create the database and 
 * tables when the application is installed
 * 
 */
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL("CREATE TABLE "+tblNotification+" (id INTEGER PRIMARY KEY AUTOINCREMENT, "
				+ message + " TEXT, " + msgDate + " TEXT default CURRENT_DATE);");
    }
    /**
     * On Update of application from one version to another version 
     * The database also gets updated to latest one
     * 
     * The old tables and values are deleted and
     * new table and data's are populated
     * 
     */
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
            int newVersion) {
        database.execSQL("DROP TABLE IF EXISTS "+tblNotification);
        onCreate(database);
    }
   
    /**
     * @param messae :: Message that push from google server
     */
    @SuppressLint("SimpleDateFormat")
	public int insertnotifications(String messae){
    	int id = 0;
    	try{
    		Log.d("messae", messae);
    		database = this.getWritableDatabase();
		    ContentValues values = new ContentValues();
		    values.put(this.message, messae);
		    
		    id = (int) database.insert(tblNotification, null, values);
    	}catch (Exception e) {
    		Log.d("insertorUpdateLoginStatus", e.getLocalizedMessage()+"");
		}
    	return id;
    }
    /**
     * 
     * Will return all the messages published through push notification
     */
    public Vector<String> getNotification()
    {
    	Vector<String> notification = new Vector<String>();
    	Log.d("", "getNotificationByAgent");
    	try{
	        database = this.getReadableDatabase(); 			   
	        Cursor cursor = database.rawQuery("Select * from "+tblNotification + " order by id desc", new String[] {});
	        Log.d("getCount", cursor.getCount()+"");
		    if ((cursor != null)&&(cursor.getCount()>0) ){
		    	cursor.moveToFirst();
		    	for(int index=0;index<cursor.getCount();index++){
					String value = cursor.getString(cursor.getColumnIndex(message));
					notification.add(value);
					cursor.moveToNext();
		    	}
			}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
	    database.close();
	    return notification;
    }
}
