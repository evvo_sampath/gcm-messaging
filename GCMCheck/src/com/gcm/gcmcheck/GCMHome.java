package com.gcm.gcmcheck;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Toast;

import com.gcm.gcmcheck.gcmlib.GcmConstants;
import com.gcm.gcmcheck.gcmlib.SendGCMMessage;

public class GCMHome extends Activity{

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.gcm_home);
		findViewById(R.id.SendGCMMSG).setOnClickListener(onclick);
		findViewById(R.id.ReceivedGCM).setOnClickListener(onclick);
	}
	OnClickListener onclick = new OnClickListener() {
		
		@Override
		public void onClick(View v) {
			Intent intent = new Intent(getApplicationContext(), SendGCMMessage.class);
			switch (v.getId()) {
			case R.id.SendGCMMSG:
				SharedPreferences sharedpreferences = getSharedPreferences(GcmConstants.PREFERENCE_NAME, Context.MODE_PRIVATE);
				intent.putExtra("register_id", sharedpreferences.getString(GcmConstants.GCM_REG_ID, ""));
				intent.putExtra("gcm_message", "a#b#c");
				startActivityForResult(intent, GcmConstants.REQUEST_SEND_GCM);
				break;
			case R.id.ReceivedGCM:
				break;
			default:
				break;
			}
		}
	};
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(resultCode == RESULT_OK){
			Toast.makeText(getApplicationContext(), "GCM message successfull...", Toast.LENGTH_LONG).show();
		}else{
			Toast.makeText(getApplicationContext(), "GCM messaging failed...", Toast.LENGTH_LONG).show();
		}
	}
}
