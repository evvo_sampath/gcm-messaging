package com.gcm.gcmcheck;

/*********************************************************************************************************
 * Class	: LandedHomesSplash
 * Type		: Activity
 * Date		: 18 Set 2013
 * 
 * Input Values
 * 	NULL
 * 
 * 	This activity shows splash screen for 2000 seconds
 * 	
 * 	After 2000 seconds this will load UserInformation activity for first time and 
 *  every time user logs-out from app and try to open next time
 * 
 * ********************************************************************************************************/

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.gcm.gcmcheck.gcmlib.GcmConstants;
import com.google.android.gcm.GCMRegistrar;

@SuppressLint("HandlerLeak")
public class SplashScreen extends Activity {

	private static final int STOPSPLASH = 0;
	private static final long SPLASHTIME = 2000;
	
	private Handler splashHandler = new Handler() {
		@Override
		public void handleMessage(Message msg) {
			switch (msg.what) {
			case STOPSPLASH:
				Intent intent = new Intent(SplashScreen.this,GCMHome.class);
				startActivity(intent);
				break;
			}
			super.handleMessage(msg);
		}
	};
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		try{
			Message msg = new Message();
			msg.what = STOPSPLASH;
			splashHandler.sendMessageDelayed(msg, SPLASHTIME);
			SharedPreferences sharedpreferences = getSharedPreferences(GcmConstants.PREFERENCE_NAME, Context.MODE_PRIVATE);
			if(!sharedpreferences.contains(GcmConstants.GCM_REG_ID)){
				GCMRegistrar.checkDevice(this);
				GCMRegistrar.checkManifest(this);
				GCMRegistrar.register(SplashScreen.this, GcmConstants.SENDER_ID);
			}
			Log.d("GCM_REG_ID", sharedpreferences.getString(GcmConstants.GCM_REG_ID, "no value"));
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Override
	public void onPause(){
		super.onPause();
		finish();
	}
}