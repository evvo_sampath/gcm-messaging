package com.gcm.gcmcheck;

import java.util.HashMap;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.gcm.gcmcheck.db.DatabaseHelper;
import com.gcm.gcmcheck.gcmlib.ConnectionCheck;
import com.gcm.gcmcheck.gcmlib.ConnectionManager;
import com.gcm.gcmcheck.gcmlib.ConnectionManager.ConnectionType;
import com.gcm.gcmcheck.gcmlib.GCMMessageView;
import com.gcm.gcmcheck.gcmlib.GcmConstants;
import com.google.android.gcm.GCMBaseIntentService;
import com.loopj.android.http.AsyncHttpResponseHandler;

public class GCMIntentService extends GCMBaseIntentService {

	public GCMIntentService() {
		super(GcmConstants.SENDER_ID);
	}

	@Override
	protected void onRegistered(Context context, String registrationId) {
		// Store device GCM registration id in preferences to access later
		SharedPreferences sharedpreferences = getSharedPreferences(GcmConstants.PREFERENCE_NAME, Context.MODE_PRIVATE);
		Editor editor = sharedpreferences.edit();
		editor.putString(GcmConstants.GCM_REG_ID, registrationId);
		Log.d("GCMIntentService", registrationId);
		// check for device registration if device registered without GCM id 
		// then update server details for this device with current GCM device registration id
		if(sharedpreferences.contains(GcmConstants.GCM_DEVICE_REGISTERED)){
			/*HashMap<String, String> postValues = new HashMap<String, String>();
			postValues.put("gcm_registration_id", registrationId);
			// Add some more post parameters to identify this device or user on this device
			postRegistration(context, postValues);
			Log.d("GCMIntentService", postValues.toString());*/
			editor.remove(GcmConstants.GCM_DEVICE_REGISTERED);
		}
		editor.commit();
		sendPushNotification(context, registrationId);
	}
	// update registration of device with gcm registration id
	private void postRegistration(Context context, HashMap<String, String> postValues){
		
		if(ConnectionCheck.checkOnline(context.getApplicationContext())){
		 ConnectionManager test = new ConnectionManager();
		  try {
				test.connectionHandler(context.getApplicationContext(),postValues,GcmConstants.GCM_UPDATE, ConnectionType.ConnectionTypePost,null, 
					new AsyncHttpResponseHandler() {
				});
			}catch(Exception e){
				// exceptions handling
			}
		}else{
			// no Internet connection
			Log.d("internect check", "no internet connection, handle this with custom dialog for settings");
		}
	}
	
	private void sendPushNotification(Context context, String registerId){
		if(ConnectionCheck.checkOnline(getApplicationContext())){
			try {
				HashMap<String, String> postValues = new HashMap<String, String>();
				postValues.put(GcmConstants.GCM_KEY, registerId);
				postValues.put(GcmConstants.GCM_MESSAGE, "Congrats your GCM check is working fine..");
				postValues.put(GcmConstants.GCM_API, GcmConstants.GCM_API_KEY);
				
				Log.d("gcmpostValues", postValues.toString());
				ConnectionManager test = new ConnectionManager();
				
				test.connectionHandler(context.getApplicationContext(), postValues, GcmConstants.NOTIFICATION, ConnectionType.ConnectionTypePost, null, 
					  new AsyncHttpResponseHandler(){
					});
			}catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			// no Internet connection
			Log.d("internect check", "no internet connection, handle this with custom dialog for settings");
		}
	}
	
	@Override
	protected void onUnregistered(Context context, String registrationId) {
		// TODO 
	}

	@Override
	protected void onMessage(Context context, Intent data) {
		String message;
		// Message from PHP server
		message = data.getStringExtra("message");
		generateNotification(context, message);
	}
	/**
     * Issues a notification to inform the user that server has sent a message.
     */
	private static void generateNotification(Context context, String message) {
      int icon = R.drawable.ic_launcher;
      long when = System.currentTimeMillis();
      NotificationManager notificationManager = (NotificationManager)
              context.getSystemService(Context.NOTIFICATION_SERVICE);
      Log.d("GCMIntentService", message);
      Intent notificationIntent = new Intent(context, GCMMessageView.class);
      // param :: agentId, Name, Msg
      DatabaseHelper db = new DatabaseHelper(context);
	  int notificationId = db.insertnotifications(message);
	    
      notificationIntent.putExtra("message", message);
      // set intent so it does not start a new activity
      notificationIntent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
      PendingIntent intent = PendingIntent.getActivity(context, notificationId, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);
      
      Notification notification = new NotificationCompat.Builder(context)
	    	.setContentText(message)
	    	.setContentTitle(context.getString(R.string.app_name))
	    	.setSmallIcon(icon)
	    	.setWhen(when)
	    	.setContentIntent(intent)
	    	.build();
	
	    notification.flags |= Notification.FLAG_AUTO_CANCEL;
	
	    // Play default notification sound
	    notification.defaults |= Notification.DEFAULT_SOUND;
	
	    // Vibrate if vibrate is enabled
	    notification.defaults |= Notification.DEFAULT_VIBRATE;
	    notificationManager.notify(notificationId, notification);
	}
	@Override
	protected void onError(Context arg0, String errorId) {
		// TODO
	}

}